import nodeutil from "util";

/** similar to rusts dbg! macro, takes a value and prints it to stdout, then returns it
 * @param {any} v - The value to inspect
 * @param {string} v - (optional) an message to print befure the value
 */
export function dbg(v: any, n?: string): any {
    console.log(`[dbg] ${n ? n : ""}=`, v);
    return v;
}

/** Deduplicate a list of strings
 * @param {string[]} a - The list of strings to deduplicate
 */
export function dedup(a: string[]): string[] {
    return [...new Set(a)];
}

export function repr(obj: any): string {
    if (typeof obj === "string") {
        return obj;
    } else if (obj?.__repr) {
        return obj.__repr();
    } else {
        return String(obj);
    }
}

/** Deduplicate a list of strings and symbols.
 * Keep in mind that this can retain symbols that are visually similar.
 * @param {(string | symbol)[]} a - The list of strings and symbols to deduplicate
 */
export function dedupSym(a: (string | symbol)[]): (string | symbol)[] {
    return [...new Set(a)];
}

const UTIL_OPTS = {
    compact: true,
    colors: true,
    showHidden: false,
    breakLength: Infinity
};


/** Debug print a variable
 * @param {any} v - The variable to print
 */
export function inspect(v: any): string {
    return nodeutil.inspect(v, UTIL_OPTS);
}
