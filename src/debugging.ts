"use strict";

import { dedupSym, inspect } from "./utils";

/** Constant name for the debug flag */
export const MAKE_DEBUG_FIELD_KEY = "__make_debug_active";

/**
 * Wrap all functions of this object with a debug representation.
 * Set the property makeDebugActive to False to supress debug messages at any time
 *
 * NOTE: This only wraps *direct* calls and makes no attempt at tracing the caller
 * */
export function makeDebug(cl: any, blacklist?: string[]) {
    if (!(cl instanceof Object)) return;
    // relative this as that
    let that = cl;

    if (blacklist === undefined || blacklist === null) blacklist = [];
    if (blacklist.indexOf("constructor") === -1) blacklist.push("constructor");

    var propNames = dedupSym(
        Reflect.ownKeys(Object.getPrototypeOf(that))
            .concat(Reflect.ownKeys(that))
    );

    for (let field of propNames.filter(
        (v: string) => typeof that[v] === "function" && blacklist?.indexOf(v) === -1
    )) {
        let target_func = that[field];
        let func = target_func.bind(that);
        let fname = field;

        let wrapper = function (...args: any[]) {
            let argstr = args.map(inspect).join(", ");
            let val = func(...args);
            if (that[Symbol.for(MAKE_DEBUG_FIELD_KEY)]) {
                console.log(
                    `[debug] call ${String(fname)}(${argstr}) => ${inspect(val)}`
                );
            }
            return val;
        };

        that[field] = wrapper;
    }

    const sym = Symbol.for(MAKE_DEBUG_FIELD_KEY);
    if (!that[sym]) {
        that[sym] = true;
    }
}
